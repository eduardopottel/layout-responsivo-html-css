// Make Cards
const sectionCards = document.querySelector("section.cards");

const card = document.querySelector("div.card");

//const cards = [...document.querySelectorAll(".cards .card")];

const artigos = [
  {
    title: "Lorem ipsum dolor sit amet",
    autor: "Lorem ipsum",
    thumb: "https://cdn.pixabay.com/photo/2018/03/27/21/43/startup-3267505_960_720.jpg",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
  },
  {
    title: "Lorem ipsum dolor sit amet",
    autor: "Lorem ipsum",
    thumb: "https://cdn.pixabay.com/photo/2015/01/20/13/13/samsung-605439_960_720.jpg",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
  },
  {
    title: "Lorem ipsum dolor sit amet",
    autor: "Lorem ipsum",
    thumb: "https://cdn.pixabay.com/photo/2015/07/17/22/42/startup-849804_960_720.jpg",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
  },
  {
    title: "Lorem ipsum dolor sit amet",
    autor: "Lorem ipsum",
    thumb: "https://cdn.pixabay.com/photo/2015/05/31/10/55/man-791049_960_720.jpg",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
  },
  {
    title: "Lorem ipsum dolor sit amet",
    autor: "Lorem ipsum",
    thumb: "https://cdn.pixabay.com/photo/2016/06/03/13/57/digital-marketing-1433427_960_720.jpg",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
  },
  {
    title: "Lorem ipsum dolor sit amet",
    autor: "Lorem ipsum",
    thumb: "https://cdn.pixabay.com/photo/2015/04/20/13/17/work-731198_960_720.jpg",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
  }
];

artigos.map(artigo => {
  const cardClone = card.cloneNode(true);
  cardClone.querySelector("img").src = artigo.thumb;
  cardClone.querySelector(".title").innerHTML = artigo.title;
  cardClone.querySelector(".description").innerHTML =
    artigo.description;
  cardClone.querySelector(".info > p.text--medium").innerHTML =
    artigo.autor;
  sectionCards.appendChild(cardClone);
});

card.remove();
